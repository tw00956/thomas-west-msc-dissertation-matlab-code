%% Function Description (Cursor Function)
% The `Cursor_Function` enhances plot interactivity by customizing data tips in MATLAB plots. It dynamically
% generates detailed tooltips that display the departure and arrival dates, the time of flight (TOF), and the delta-v
% (∆V) based on the cursor's position over plot data points. This function is typically used to provide additional
% context for data points in plots related to time series or trajectory analyses, improving data readability and
% user interaction.

% Author: Thomas West
% Date: April 18, 2024

%% Function
function output_txt = Cursor_Function(obj, event_obj)
    % Customizes text of data tips
    pos = get(event_obj, 'Position');  % Get the position of the cursor

    % Convert serial date number to MATLAB date number
    departure_date = datenum('2000-01-01 12:00:00') + (pos(1) - 730486); 

    % Convert the numeric date to a string format
    departure_str = datestr(departure_date, 'dd-mmm-yyyy');  

    tof_days = pos(2);  % Time of Flight in days

    % Calculate arrival date by adding TOF to departure date
    arrival_date = addtodate(departure_date, round(tof_days), 'day');  

    % Convert arrival date to string format
    arrival_str = datestr(arrival_date, 'dd-mmm-yyyy');  

    % Format output text to display in the data tip
    output_txt = {...
        ['Departure Date: ', departure_str], ...
        ['TOF: ', num2str(tof_days, '%.1f'), ' days'], ...
        ['Arrival Date: ', arrival_str], ...
        ['∆V: ', num2str(pos(3), '%.5g'), ' km/s']...
    };
end