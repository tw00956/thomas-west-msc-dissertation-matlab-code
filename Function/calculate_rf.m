function rf = calculate_rf(rp, Omega, i, omega)
    % Initial periapsis vector in orbital plane
    r_peri = [rp * cosd(omega); rp * sind(omega); 0];
    
    % Rotation matrix for inclination
    R_i = [1 0 0; 0 cosd(i) -sind(i); 0 sind(i) cosd(i)];
    
    % Rotation matrix for RAAN
    R_Omega = [cosd(Omega) -sind(Omega) 0; sind(Omega) cosd(Omega) 0; 0 0 1];
    
    % Apply rotations
    r_inclined = R_i * r_peri;  % Rotate by inclination
    rf = R_Omega * r_inclined;  % Rotate by RAAN
end
