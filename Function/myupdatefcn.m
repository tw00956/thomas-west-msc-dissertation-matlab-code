function txt = myupdatefcn(~, event_obj, positions, velocities)
    % Customizes text of data tips
    pos = get(event_obj,'Position');  % Get the position of the data tip
    idx = find(ismember(positions, pos, 'rows'));  % Find the index of the position in the matrix

    if isempty(idx)
        txt = {'Position:', pos, 'Velocity: Not found'};
    else
        vel = velocities(idx, :);  % Extract corresponding velocity
        vel_mag = norm(vel);  % Calculate the magnitude of the velocity vector
        txt = {['Position: (', num2str(pos(1)), ', ', num2str(pos(2)), ', ', num2str(pos(3)), ')'], ...
               ['Velocity: (', num2str(vel(1)), ', ', num2str(vel(2)), ', ', num2str(vel(3)), ')'], ...
               ['Velocity Magnitude: ', num2str(vel_mag), ' m/s']};  % Add magnitude to the data tip
    end
end