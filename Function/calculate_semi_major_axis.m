function a = calculate_semi_major_axis(rp, e)
    % Calculate the semi-major axis from periapsis and eccentricity
    a = rp / (1 - e);
end