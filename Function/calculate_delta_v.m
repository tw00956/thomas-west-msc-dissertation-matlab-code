%% Function Description (Calculate Delta V)
% The function `calculate_delta_v` computes the total delta-v required for interplanetary transfers between Earth and Mars,
% given specific departure and arrival velocities as well as the corresponding positions of the two planets. This function
% is particularly useful in mission design, allowing the analysis of feasible launch windows and transfer opportunities by 
% calculating the required propulsive change in velocity (delta-v) at both departure and arrival.
%
% The function iterates over a range of potential departure days and times of flight (TOF), calculating delta-v for each combination.
% The departure days are specified over a range (`num_departure_days`), and the time of flight is explored within given bounds
% (`TOF_min` to `TOF_min + num_TOF_days - 1` with an increment `dTOF`). For each day and TOF, the function uses provided initial
% and final velocities (`V0`, `Vf`) from a Lambert solver along with the position vectors of Earth (`Xearth`) and Mars (`Xmars`) 
% at those specific times to compute the delta-v at departure and arrival. The result is filtered to exclude combinations where
% the total delta-v exceeds a specified maximum (`Max_v`).
%
% Inputs:
% - V0: A 3D matrix of initial velocities from Earth, indexed by departure day and TOF.
% - Vf: A 3D matrix of final velocities at Mars, matching the dimensions of V0.
% - Xearth: A matrix containing Earth's position and velocity vectors over potential departure days.
% - Xmars: A 3D matrix containing Mars' position and velocity vectors, indexed by departure day and TOF.
% - Max_v: The maximum allowable total delta-v for the mission (used as a filter criterion).
% - num_departure_days: The number of days considered for departure.
% - num_TOF_days: The number of different TOF values considered.
% - TOF_min: The minimum time of flight considered.
% - dTOF: The increment in time of flight between each TOF considered.
%
% Output:
% - delta_v_matrix: A matrix of total delta-v values for each departure day and TOF combination. NaN values represent combinations
% that are either not computable or exceed the maximum delta-v threshold.
%
% Author: Thomas West
% Date: April 18, 2024

%% Function
function delta_v_matrix = calculate_delta_v(V0, Vf, Xearth, Xmars, Max_v, num_departure_days, num_TOF_days, TOF_min, dTOF)
    % Initialize the delta_v_matrix with NaN for all entries to handle unprocessed or invalid entries.
    delta_v_matrix = NaN(num_departure_days, num_TOF_days);

    for d = 1:num_departure_days   % Loop over each possible departure day
        for tof = TOF_min:dTOF:(TOF_min + num_TOF_days - 1) % Loop over each possible Time of Flight (TOF)
            tof_index = (tof - TOF_min) / dTOF + 1;  % Calculate the index for the current TOF in the matrices V0 and Vf

            % Check if the calculated indices for departure day 'd' and TOF 'tof_index' are within the valid range of V0 and Vf matrices
            if tof_index > size(V0, 3) || d > size(V0, 2)
                fprintf('Skipping invalid index d=%d, tof_index=%d\n', d, tof_index); % Output a message indicating skipping of iteration
                continue;  % Skip the current iteration if indices are out of bounds
            end

            % Retrieve the Earth's position vector on the departure day 'd' from matrix Xearth
            r0 = Xearth(1:3, d);  
            % Retrieve Mars' position vector on the arrival day corresponding to 'd' and 'tof_index' from matrix Xmars
            rf = Xmars(1:3, d, tof_index);  
            % Retrieve the initial velocity vector from the Lambert solver at departure day 'd' and TOF 'tof_index' from matrix V0
            v0 = V0(:, d, tof_index);  
            % Retrieve the final velocity vector from the Lambert solver at arrival from matrix Vf
            vf = Vf(:, d, tof_index);  

            if all(~isnan(v0)) && all(~isnan(vf))  % Check if there are no NaN values in the initial and final velocity vectors
                % Calculate the departure delta-v by finding the norm of the difference between initial velocity and Earth's velocity
                delta_v1 = norm(v0 - Xearth(4:6, d));  
                % Calculate the arrival delta-v by finding the norm of the difference between final velocity and Mars' velocity
                delta_v2 = norm(vf - Xmars(4:6, d, tof_index));  
                % Sum the departure and arrival delta-v values to get the total delta-v for the trajectory
                total_delta_v = delta_v1 + delta_v2;

                % Check if the total delta-v is within the maximum allowable delta-v
                if total_delta_v <= Max_v
                    % Assign the total delta-v to the corresponding cell in the delta_v_matrix
                    delta_v_matrix(d, tof_index) = total_delta_v;
                end
            end
        end
    end
end