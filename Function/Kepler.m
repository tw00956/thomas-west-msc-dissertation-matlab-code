% (W2.1)

function [E, no_of_iterations] = Kepler(e, M, tol)

% Initialize the estimate of E with M
E = M;

% Set a maximum number of iterations
max_iterations = 100;
for ii = 1:max_iterations

    f = M - E + e * sin(E);           % evaluate function with respect to E
    fp = -1 + e * cos(E);             % evaluate first derivative with respect E
  
    % Print output
    % fprintf('E: %f, |f(E)|: %e\n', E, abs(f));

    % Note: Un-note the above code to see all the convergencies, it is
    % currently noted as it computes a lot of unnecessary data into the
    % command window. The script still outputs the correct value of 'E’.


    % check for convergence using IF statement
    if(abs(f) < tol)
        break;
    else
        % Newton's method
        dE = -f/fp;                          % delta E
        E = E + dE;                          % update estimate of E
    end
end

no_of_iterations = ii;

end