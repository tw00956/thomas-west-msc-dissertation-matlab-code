%% Function Description (True Anomaly)
% The function `trueAnomaly` calculates the true anomaly (nu) for an orbiting body given its position vector `r`,
% velocity vector `v`, and the gravitational parameter `mu` of the central body. The true anomaly is the angle between 
% the perihelion (the point of the orbit closest to the central body) and the current position of the body as seen from 
% the focus of the ellipse.
%
% This function performs the following calculations:
% - **Specific Angular Momentum (`h`)**: Computed as the cross product of the position vector and the velocity vector. 
%   This vector is perpendicular to the plane of motion and its magnitude gives the areal velocity.
% - **Eccentricity Vector (`e_vec`)**: Derived from the formula `(cross(v, h) / mu) - (r / norm(r))`. This vector points 
%   from the focus of the orbit (central body) to the perihelion and its magnitude is the orbital eccentricity, `e`.
% - **True Anomaly (`nu`)**: Calculated using the eccentricity vector and the position vector. The cosine of the true anomaly 
%   is determined by the dot product of the eccentricity vector and the position vector, normalized by their magnitudes. 
%   The sine component is calculated using the dot product of the angular momentum vector and the cross product of the 
%   eccentricity vector and the position vector, also normalized by their magnitudes. `atan2` function is used to compute 
%   the angle from the cosine and sine values ensuring the correct quadrant for the angle.
%
% The true anomaly is a critical parameter in orbital mechanics as it describes the position of the orbiting body along its 
% orbit at any given time. It is particularly useful in the analysis and prediction of the orbital paths and for mission planning.
%
% Inputs:
% - r: Position vector of the orbiting body (3-element vector).
% - v: Velocity vector of the orbiting body at the same instant as `r` (3-element vector).
% - mu: Gravitational parameter of the central body (scalar).
%
% Output:
% - nu: True anomaly in radians (scalar).
%
% Author: Thomas West
% Date: April 18, 2024

%% Function
function nu = trueAnomaly(r, v, mu)
    % Calculate specific angular momentum
    % 'h' is computed as the cross product of position vector 'r' and velocity vector 'v'.
    % This vector 'h' is perpendicular to the plane of the orbit and represents the angular momentum of the body.
    h = cross(r, v);

    % Calculate eccentricity vector
    % 'e_vec' is calculated from two components:
    % 1. The cross product of velocity vector 'v' and angular momentum 'h', scaled by the gravitational parameter 'mu'.
    % 2. The position vector 'r' scaled by its magnitude (norm(r)), which normalizes it.
    % The resulting vector 'e_vec' points from the focus of the ellipse (near the central body) to the perihelion.
    e_vec = (cross(v, h) / mu) - (r / norm(r));

    % Compute the magnitude of the eccentricity vector to get the scalar eccentricity 'e'.
    e = norm(e_vec);

    % Calculate true anomaly
    % 'cos_nu' is the cosine of the true anomaly, computed as the dot product of 'e_vec' and 'r',
    % normalized by the product of their magnitudes (e * norm(r)).
    cos_nu = dot(e_vec, r) / (e * norm(r));

    % 'sin_nu' is the sine of the true anomaly, calculated using:
    % 1. The dot product between 'h' and the cross product of 'e_vec' and 'r',
    % 2. Normalized by the product of 'e', the norm of 'h', and the norm of 'r'.
    sin_nu = dot(h, cross(e_vec, r)) / (e * norm(h) * norm(r));

    % The true anomaly 'nu' is then determined using the 'atan2' function,
    % which calculates the arc tangent of 'sin_nu' and 'cos_nu', providing the correct quadrant for the angle.
    nu = atan2(sin_nu, cos_nu);
end
