function [RIJK, VIJK] = COE2RV_Elliptic(A, MU)
    % Parse out orbital elements
    semi = A(1);  % Semi-major axis
    e = A(2);     % Eccentricity
    i = A(3);     % Inclination (radians)
    node = A(4);  % Right Ascension of Ascending Node (RAAN) (radians)
    arg = A(5);   % Argument of Perigee (radians)
    trueAnomaly = A(6);  % True Anomaly (radians)
   
    % Calculate the semi-latus rectum (semiparameter)
    p = semi * (1 - e^2);
   
    % Position Coordinates in Perifocal Coordinate System
    RPQW = [p * cos(trueAnomaly) / (1 + e * cos(trueAnomaly));  % x-coordinate (km)
            p * sin(trueAnomaly) / (1 + e * cos(trueAnomaly));  % y-coordinate (km)
            0];                                                % z-coordinate (km)
   
    % Velocity Coordinates in Perifocal Coordinate System
    VPQW = [-sqrt(MU / p) * sin(trueAnomaly);          % velocity in x (km/s)
             sqrt(MU / p) * (e + cos(trueAnomaly));    % velocity in y (km/s)
             0];                                       % velocity in z (km/s)
   
    % Transformation Matrix (3 Rotations)
    rot = (rot3(-node) * rot1(-i) * rot3(-arg));

    % Transforming Perifocal -> ECI
    RIJK = rot * RPQW;
    VIJK = rot * VPQW;
end

% Auxiliary functions for rotations about axes
function R = rot3(theta)
    R = [cos(theta), sin(theta), 0; -sin(theta), cos(theta), 0; 0, 0, 1];
end

function R = rot1(theta)
    R = [1, 0, 0; 0, cos(theta), sin(theta); 0, -sin(theta), cos(theta)];
end
